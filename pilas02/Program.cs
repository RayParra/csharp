﻿using System;
using System.Collections;

namespace pilas02
{
    class Program
    {
        static void Main(string[] args)
        {
            var dato = "";


            Console.WriteLine("Manejo Basico de los Stack!");
            Stack pila = new Stack();
            Stack pila2 = new Stack();
            dato  = Console.ReadLine();
            pila.Push(dato);
            Console.WriteLine(pila.Peek());
            pila.Push("Joe Done");

            Console.WriteLine($"el numero de elementos es {pila.Count}");
        
            pila.Clear();
            pila.Push(23);

            Console.WriteLine($"el numero de elementos despues del clear es:  {pila.Count}");

        }
    }
}
