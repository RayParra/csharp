﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace queue_example
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declarar (crear) la cola (clase Queue)
            var cola = new Queue();
            cola.Enqueue("cliente 1");
            Console.WriteLine("Elementos en la Queue: ");
            foreach (var item in cola)
            {
                Console.WriteLine($"Elemento: {item}");
            }

            Console.WriteLine($"El numero de Elementos en la Queue {cola.Count}");
            Console.WriteLine($"El primer Elemento en la Queue {cola.Peek()}");
            cola.Dequeue();
            
            Console.WriteLine("Elementos fuera de la Queue: ");
            foreach (var item in cola)
            {
                Console.WriteLine($"Elemento: {item}");
            }
        }
    }
}
