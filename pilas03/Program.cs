﻿using System;
using System.Collections;

namespace pilas03
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack pila = new Stack();
            pila.Push("Helllo");
            pila.Push("World");
            pila.Push("!");
            
            Console.WriteLine("Contando la Pila!");
            Console.WriteLine($"El numero de elementos de la pila son: {pila.Count}");
            PrintValues(pila);
        }

        public static void PrintValues(IEnumerable myCollection){
            foreach (Object item in myCollection)
            {
                Console.WriteLine($"Item: {item}");
                Console.WriteLine();
            }
        }
    }
}
