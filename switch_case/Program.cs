﻿using System;

namespace switch_case
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            int opc;

            opc = r.Next(8);
            switch (opc)
            {
                case 1: Console.WriteLine("El dia sorteado es Lunes"); break;
                case 2: Console.WriteLine("El dia sorteado es Martes"); break;
                case 3: Console.WriteLine("El dia sorteado es Miercoles"); break;
                case 4: Console.WriteLine("El dia sorteado es Jueves"); break;
                case 5: Console.WriteLine("El dia sorteado es Viernes"); break;
                case 6: Console.WriteLine("El dia sorteado es Sabado"); break;
                case 7: Console.WriteLine("El dia sorteado es Domingo"); break;
                default: Console.WriteLine("Dia no existente."); break;
            }
        }
    }
}
