﻿using System;
using System.Collections;

namespace pilas
{
    class Program
    {

        static void Main(string[] args)
        {
            
           var pila = new Stack();
           pila.Push("Ray");
           pila.Push("Parra");

           foreach (var item in pila)
           {
               Console.WriteLine(item);
           }
           Console.WriteLine(pila.Peek());
        }
    }
}
