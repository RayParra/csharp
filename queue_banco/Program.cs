﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace queue_banco
{
    class Program
    {

        public static  void  menu(ref Queue cola){
            string opc;
            Random r = new Random();
            int operacion;
            
            Console.WriteLine("Menu! ");
            Console.WriteLine("1: Agregar Turno!");
            Console.WriteLine("2: Atender Turno!");
            Console.WriteLine("3: Lista de Turnos");
            Console.WriteLine("3: Salir");
            Console.WriteLine("Seleccione una Opcion del Menu: ");
            opc = Console.ReadLine();
           
            switch (opc)
            {
                case "1": 
                    operacion = r.Next(11);
                    cola.Enqueue(operacion); 
                    Console.WriteLine($"El turno para la operacion {operacion} esta en espera!");
                    menu(ref cola);
                break;
                case "2":
                    int cajero = r.Next(5);
                    Console.WriteLine($"Turno en Atencion: {cola.Peek()} lo atiende caja {cajero}");
                    cola.Dequeue();
                    menu(ref cola);
                break;
                case "3":
                    Console.WriteLine("Turnos en Espera! ");
                    foreach (var turnos in cola)
                    {
                        Console.WriteLine(turnos);
                    } 
                    menu(ref cola);
                break;
                case "4": Console.WriteLine("Gracias por su preferencia! "); break;
                default: Console.WriteLine("Funcion no Disponible!"); break;
            }

        }

        static void Main(string[] args)
        {
            Queue cola = new Queue();
            menu(ref cola);
            
            
        }
    }
}
